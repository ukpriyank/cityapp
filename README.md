# CityApp



## Getting started

Clone the Repository and install helm Chart.

## Application Endpoints.


```
http://NODE_IP:NODE_PORT/v1/citydata/
http://NODE_IP:NODE_PORT/v1/citydata/addcity
http://NODE_IP:NODE_PORT/v1/citydata/listcities
http://NODE_IP:NODE_PORT/v1/citydata/listcity/<CityName>
http://NODE_IP:NODE_PORT/v1/citydata/updatecity/<CityName>
http://NODE_IP:NODE_PORT/v1/citydata/healthcheck
```



## Installation

```
git clone https://gitlab.com/ukpriyank/cityapp.git
helm install <Name> g42
```

## Customized values in Values.yaml

vi g42/values.yaml ( in App and Database Sections )

## Docker Images
Docker Image created with DockerFile which has base python:alpine3.16 and install django and required dependencies into to it.

```
docker build -f DockerFile -t priyankuk/g42:latest . 
```
## Code

Code has the Django Application settings 
***

# Database
Application is MySQL Database container, We can setup this container with the Volumes but for this Demo/testing instance made it simple.

Application response will start after the Database initialization. (i.e 1-2 minutes)

## Name
Choose a self-explaining name for your project.

## Dependency
Application requires Kubernaties cluster/ Minikube setup and Helm installation.
