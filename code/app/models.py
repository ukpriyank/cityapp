from django.db import models

# Create your models here.

class Population(models.Model):
    City = models.CharField(max_length=100, unique=True)
    Population = models.IntegerField()

    def __str__(self):
        return  self.City