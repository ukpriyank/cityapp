from django.conf.urls import url
from . import views
from django.urls import path


urlpatterns = [
   path('addcity/', views.AddCity.as_view()),
   path('listcities/', views.ListAllCities.as_view()),
   url(r'^listcity/(?P<City>\w+(\W\w+)*)/$', views.ListCity.as_view()),
   url(r'^updatecity/(?P<City>\w+(\W\w+)*)/$', views.UpdateCity.as_view()),
   path('healthcheck/', views.HealthCheck.as_view()),   
]
