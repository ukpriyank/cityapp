from django.shortcuts import render

# Create your views here.


from .models import *
from rest_framework import generics
from .serializers import PopulationSerializer
from django.http.response import JsonResponse


class AddCity(generics.CreateAPIView):
    queryset = Population.objects.all()
    serializer_class = PopulationSerializer


class ListAllCities(generics.ListAPIView):
    queryset = Population.objects.all()
    serializer_class = PopulationSerializer


class ListCity(generics.RetrieveAPIView):
    queryset = Population.objects.all()
    serializer_class = PopulationSerializer
    lookup_field = 'City'


class UpdateCity(generics.RetrieveUpdateAPIView):
    queryset = Population.objects.all()
    serializer_class = PopulationSerializer
    lookup_field = 'City'

class HealthCheck(generics.ListAPIView):
    def get(self, request):
        # ...
        # Business Logic
        # ...
        return JsonResponse('OK', safe=False)

