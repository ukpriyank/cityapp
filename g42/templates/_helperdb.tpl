
{{/*
Common labels Db
*/}}
{{- define "g42.labelsdb" -}}
helm.sh/chart: {{ include "g42.chart" . }}
{{ include "g42.selectorLabelsDb" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}


{{/*
Selector labels DB
*/}}
{{- define "g42.selectorLabelsDb" -}}
app.kubernetes.io/name: {{ include "g42.name" . }}-{{ .Values.Databases.add }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}